from backend.models import Users, SubUsers, Approximations, Records
from flask_bcrypt import Bcrypt
from flask_sqlalchemy import SQLAlchemy
import uuid
import logging

bcrypt = Bcrypt()

logger = logging.getLogger('operations')

handler = logging.StreamHandler()

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)

logger.addHandler(handler)

"""
CRUD Functions
"""


# Returns user data from a given email and password
# If the stored password hash it's not valid returns false, None if any error on querying the database
# Serializes the result in JSON format
def get_user(email: str, password: str):
    try:
        user = Users.query.filter_by(email=email).first()

        if not _validate_user_password(password, user.password):
            return False

        return user.serialize
    except Exception as e:
        logger.exception(e)
        return None


# Stores a new user in the database
def post_user(database: SQLAlchemy, username: str, email: str, password: str, privileges: str):
    try:
        hashed_password = bcrypt.generate_password_hash(password.encode('utf-8'))

        # pgsql stores the hashed password in a weird way, so it must be decoded 
        new_user = Users(idUser=uuid.uuid4(), username=username, email=email, password=hashed_password.decode('utf-8'),
                         privileges=privileges)
        database.session.add(new_user)
        database.session.commit()
    except Exception as e:
        logger.exception(e)
        return False

    return True


# Gets the subusers of a given user email and creates a dictionary with the result 
# Where the key is the ID and the value it's the subuser related data
# Serializes the result in JSON format
def get_user_subusers(user_uuid: str):
    try:
        subusers = SubUsers.query.with_entities(SubUsers).join(Users, SubUsers.idUser == Users.idUser).filter(
            Users.idUser == user_uuid).all()

        subusers_dict = {subuser.idSubUser: subuser.serialize for subuser in subusers}

        return subusers_dict

    except Exception as e:
        logger.exception(e)
        return None


# Creates a new subuser in the database
# Returns False if any error in the transaction, else returns true
def post_subuser(database: SQLAlchemy, new_subuser: SubUsers):
    try:
        hashed_password = bcrypt.generate_password_hash(new_subuser.password.encode('utf-8'))

        # Same way as users password
        new_subuser.password = hashed_password.decode('utf-8')
        database.session.add(new_subuser)
        database.session.commit()
    except Exception as e:
        logger.exception(e)
        return False

    return True


# Gets all the approximations of the given user email from the database
# Converts the result into a dictionary where the key is the ID and the value the approximation data related
# Serializes the result in JSON format
def get_user_approximations(user_uuid: str):
    try:
        approximations = Approximations.query.with_entities(Approximations).join(Users,
            Approximations.idUser == Users.idUser).filter(
            Users.idUser == user_uuid).all()

        approximations_dict = {
            approximation.idAprox: (approximation.serialized, _get_approximation_records(approximation.idAprox)) for
            approximation in approximations}

        return approximations_dict

    except Exception as e:
        logger.exception(e)
        return None


# Stores a new approximation in the database
# Assign the new id to the records that correspond to the approximation and stores it
def post_user_approximation(database: SQLAlchemy, approximation: Approximations, records: [Records]):
    try:
        database.session.add(approximation)
        database.session.commit()

        [record.__setattr__('idAprox', approximation.idAprox) for record in records]

        database.session.add_all(records)
        database.session.commit()
    except Exception as e:
        logger.exception(e)
        return False

    return True


# Gets all the records from the database of the given user email
# Serializes the result in JSON format
def get_user_records(user_uuid: str):
    try:
        records = Records.query.with_entities(Records.recordname, Records.unitCost, Records.description,
                                              Records.recordType).join(Approximations,
                                                                       Approximations.idAprox == Records.idAprox).join(
            Users, Users.idUser == Approximations.idUser).filter(Users.idUser == user_uuid).distinct().all()

        records_dict = {index: record.serialize for index, record in enumerate(records)}

        return records_dict
    except Exception as e:
        logger.exception(e)
        return None

"""
Helper functions
"""


# Validates a given password and a hash with flask bcrypt utility
def _validate_user_password(input_password: str, hashed_password: str):
    return bcrypt.check_password_hash(hashed_password, input_password)


# Gets the approximation related records from a given id
# Serializes the result into a JSON format
def _get_approximation_records(id_approximation: int):
    records = Records.query.with_entities(Records).join(Approximations,
                                                        Records.idAprox == Approximations.idAprox).filter(
        Approximations.idAprox == id_approximation).all()

    serialized_records = [record.serialize for record in records]

    return serialized_records
